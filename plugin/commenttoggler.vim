" Comment the range of lines specified
function! s:CommentRange(from, to)
    silent execute a:from . ',' . a:to . 's?.*?\=printf(&commentstring, submatch(0))?'
endfunction

" Uncomment the range of lines specified
function! s:UncommentRange(from, to)
    " Make sure that if there is a * in the comment text, it changes to \* for
    " the regex we will be running
    let cmd = substitute(&commentstring, '*', '\\*', 'g')
    " Change %s to \(.*\)
    let cmd = substitute(cmd, '%s', '\\(.*\\)', '')
    " Build sub regex
    let cmd = a:from . "," . a:to . "s?^" . cmd . '$?\1?'
    " Filter text selected by motion through search/replace
    silent execute cmd
endfunction

" Returns true if the line number specified is commented out
function! s:IsComment(line)
    " Does this work if the comment delimiter is not in first char?
    return (synIDattr(synIDtrans(synID(a:line, 1, 0)), 'name') != 'Comment')
endfunction

" Detect if the selected block is currently *entirely* commented out -
" if so, then uncomment, otherwise comment
function! ToggleCommentBlock(type, ...)
    " Remember register contents
    let reg_save = @@

    if a:0
        " Visual mode
        let from    = line("'<")
        let to      = line("'>")
    elseif a:type == 'line' || a:type == 'char'
        " Motion mode
        let from    = line("'[")
        let to      = line("']")
    else
        echo 'Unsupported motion (' . a:type . ') ... so far'
        return
    endif

    if s:GrepRange(from, to, function('s:IsComment'))
        call s:CommentRange(from, to)
    else
        call s:UncommentRange(from, to)
    endif

    " Restore
    let @@ = reg_save
endfunction

" Returns true if *any* line in the specified range returns true when passed to
" 'myfunction' (lazily evaluated)
function! s:GrepRange(fromLine, toLine, myFunction, ...)
    let line = a:fromLine
    while line <= a:toLine
        if a:myFunction(line)
            " Grep found a match
            return 1
        endif
        let line = line + 1
    endwhile
    " Grep failed to find a match
    return 0
endfunction
